"use strict";

// Getting the parent class.
var Bridge = require('./lib-bridge');

var util = Bridge.util, logs = Bridge.log;

/* Package Installer Class */
class Installer extends Bridge {
    // Installer
    install ( pkgs, scope ) {
        var $this = this;

        scope = !scope ? 'usr' : scope;

        util.loop(pkgs, function ( pkg ) {
            pkg = $this.splitvr(pkg, $this);

            logs(['cyan', 'Installing'], '')
        });
    }

    // Pre-Install
    _install ( pkg, scope ) {

    }

    // Post Install
    install_ ( pkg, scope ) {

    }

    // Register
    register ( pkg, scope ) {

    }

    /**
     * NPM Install
     * Run NPM install to install package and its dependencies.
     *
     * @param name      - Package Name
     * @param version   - Package Version
     * @param scope     - The Bridge Object.
     */
    static npminstall ( name, version, scope ) {

    }
}

// Wrapping the class it self.
var self = Installer;

var test = new Installer();
test.install([ 'native-js@^3.3.0', 'cli-color' ], 'usr');

module.exports = Installer;
